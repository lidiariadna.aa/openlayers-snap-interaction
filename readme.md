# OpenLayers / Snap Interaction

_OL_ ofrece un conjunto de interacciones prediseñadas para facilitar la creación de aplicaciones de mapas web interactivas. Estas interacciones son subclases de `ol/interaction/Interaction` y se encuentran dentro del espacio de `ol/interaction.js`. Entre estas interacciones, se encuentran:

- `Draw`: Permite a los usuarios dibujar diferentes geometrías como puntos, líneas, polígonos y círculos en el mapa.
- `Modify`: Habilita la modificación de las características existentes permitiendo cambiar su forma o tamaño.
- `Select`: Facilita la selección una o más características en el mapa.
- `Snap`: Proporciona funcionalidad de snap (ajuste) a características cercanas durante la edición o el dibujo, para asegurar la precisión.

Estas subclases de interacción permiten crear mapas interactivos, mejorando la experiencia del usuario final. Al combinar y configurar estas interacciones, se pueden lograr comportamientos personalizados en aplicaciones GIS y de mapeo web.

En este ejemplo iniciaremos estableciendo las bases para el mapainteractivo al crear por dos capas distintas: una capa raster, que funcionará como el mapa baseproporcionando el contexto geográfico, y una capa vectorial, diseñada para manejar las interacciones con las geometrías específicas. Una vez definidas estas capas, se inicializará el mapa, configurando sus capas, el objetivo de visualización y la vista inicial. Esta estructura nos permitirá integrar funcionalidades interactivas de manera efectiva.

```js
const map = new Map({
  layers: [raster, vector],
  target: 'map',
  view: new View({
    center: [-11035828.104, 2206160.1144],
    zoom: 4,
  }),
});
```
Ahora se crearán dos objetos, `ExampleModify` y `ExampleDraw`, para configurar las interacciones de _modificación_ y _dibujo_, respectivamente.

#### Interacción de Modificación (ExampleModify)

Primero:

- Se crea una interacción `Select` para seleccionar características en el mapa.
- Se añade una interacción `Modify`, vinculada a las características seleccionadas, permitiendo su modificación.
- Se definen eventos para manejar la activación y desactivación de estas interacciones.

```js
onst ExampleModify = {
  init: function () {
    this.select = new Select();
    map.addInteraction(this.select);

    this.modify = new Modify({
      features: this.select.getFeatures(),
    });
    map.addInteraction(this.modify);

    this.setEvents();
  },
  setEvents: function () {
    const selectedFeatures = this.select.getFeatures();

    this.select.on('change:active', function () {
      selectedFeatures.forEach(function (each) {
        selectedFeatures.remove(each);
      });
    });
  },
  setActive: function (active) {
    this.select.setActive(active);
    this.modify.setActive(active);
  },
};
ExampleModify.init();
```
#### Interacción de Dibujo (ExampleDraw)

Ahora:

- Se agregan interacciones `Draw` para diferentes tipos de geometrías (punto, línea, polígono, círculo) pero inicialmente desactivadas.
- Se proporciona una función `setActive` para activar el tipo de dibujo deseado según la selección del usuario.


```js
const ExampleDraw = {
  init: function () {
    //
  },
  setActive: function (active) {
    //
  },
};
ExampleDraw.init();
```
Se maneja el cambio entre las interacciones de dibujo y modificación mediante eventos en un formulario, permitiendo al usuario elegir entre dibujar nuevas características o modificar las existentes:

```js
optionsForm.onchange = function (e) {
```
Finalmente, se añade una interacción `Snap` que se asegura de que tanto las nuevas geometrías dibujadas como las modificaciones a características existentes se ajusten a las geometrías cercanas:

```js 
const snap = new Snap({
  source: vector.getSource(),
});
map.addInteraction(snap);
```
Este enfoque permite crear una aplicación de mapeo web interactiva y precisa, facilitando tanto la creación como la modificación de datos espaciales con herramientas intuitivas de dibujo y edición.


